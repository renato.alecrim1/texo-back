package renato.alecrim.texo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import renato.alecrim.texo.dto.MinMaxWinIntervalDTO;

import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TexoApplicationTests {

	@LocalServerPort
	private int port;

	@Test
	void testMinMaxProducersIntervalWins() {
		String url = "http://localhost:" + port + "/public/producer/max-min-win-interval-for-producers";
		var response = new TestRestTemplate().getForEntity(url, MinMaxWinIntervalDTO.class);

		Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
		Assertions.assertNotNull(response.getBody());
		Assertions.assertNotNull(response.getBody().getMax());
		Assertions.assertNotNull(response.getBody().getMin());
		Assertions.assertTrue(response.getBody().getMin().get(0).getInterval() > 0);
		Assertions.assertTrue(response.getBody().getMax().get(0).getInterval() > 0);
	}

	@Test
	void testMoviesRequest() {
		String url = "http://localhost:" + port + "/public/movie/movies";
		var response = new TestRestTemplate().getForEntity(url, List.class);

		Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
		Assertions.assertNotNull(response.getBody());
	}

	@Test
	void testProducersRequest() {
		String url = "http://localhost:" + port + "/public/producer/producers";
		var response = new TestRestTemplate().getForEntity(url, List.class);

		Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
		Assertions.assertNotNull(response.getBody());
	}

	@Test
	void testStudioRequest() {
		String url = "http://localhost:" + port + "/public/studio/studios";
		var response = new TestRestTemplate().getForEntity(url, List.class);

		Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
		Assertions.assertNotNull(response.getBody());
	}
}
