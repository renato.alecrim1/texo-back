package renato.alecrim.texo.config;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import renato.alecrim.texo.domain.Movie;
import renato.alecrim.texo.domain.Producer;
import renato.alecrim.texo.domain.Studio;
import renato.alecrim.texo.dto.MovieDTO;
import renato.alecrim.texo.repository.MovieRepository;
import renato.alecrim.texo.repository.ProducerRepository;
import renato.alecrim.texo.repository.StudioRepository;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class DataLoader implements ApplicationRunner {
    private final MovieRepository movieRepository;
    private final StudioRepository studioRepository;
    private final ProducerRepository producerRepository;

    @Autowired
    public DataLoader(MovieRepository movieRepository, StudioRepository studioRepository, ProducerRepository producerRepository) {
        this.movieRepository = movieRepository;
        this.studioRepository = studioRepository;
        this.producerRepository = producerRepository;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void run(ApplicationArguments args) throws Exception {
        var moviesDTO = readMovies();

        var studios = saveStudios(moviesDTO);
        var producers = saveProducer(moviesDTO);

        var movies = moviesDTO.parallelStream()
                .map(movieDTO -> new Movie(movieDTO, studios, producers))
                .toList();

        movieRepository.saveAll(movies);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    private List<Studio> saveStudios(List<MovieDTO> movies) {
        var studios = movies.parallelStream()
                .map(MovieDTO::getStudios)
                .flatMap(List::stream)
                .collect(Collectors.toSet())
                .parallelStream()
                .map(Studio::new)
                .toList();

        return this.studioRepository.saveAll(studios);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    private List<Producer> saveProducer(List<MovieDTO> movies) {
        var producers = movies.parallelStream()
                .map(MovieDTO::getProducers)
                .flatMap(List::stream)
                .collect(Collectors.toSet())
                .parallelStream()
                .map(Producer::new)
                .toList();

        return this.producerRepository.saveAll(producers);
    }

    private List<MovieDTO> readMovies() throws IOException, CsvValidationException {

        var classPathResource = new ClassPathResource("movielist.csv");

        var inputReader = new InputStreamReader(classPathResource.getInputStream());

        var parserBuilder = new CSVParserBuilder().withSeparator(';').build();

        try (var readerBuilder = new CSVReaderBuilder(inputReader).withSkipLines(1).withCSVParser(parserBuilder).build()) {
            var movies = new ArrayList<MovieDTO>();

            String[] row;
            while (nonNull(row = readerBuilder.readNext())) {
                movies.add(new MovieDTO(row));
            }

            return movies;
        }
    }

}
