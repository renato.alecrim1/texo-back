package renato.alecrim.texo.dto;

public interface ProducerWinsDTO {
    Integer getId();
    String getName();
    String getYearsWon();
}
