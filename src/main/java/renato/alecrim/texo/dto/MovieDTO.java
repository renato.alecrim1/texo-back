package renato.alecrim.texo.dto;

import java.util.Arrays;
import java.util.List;

public class MovieDTO {
    private Integer year;
    private String title;
    private List<String> studios;
    private List<String> producers;
    private Boolean winner;

    public MovieDTO(String[] row) {
        year = Integer.parseInt(row[0]);
        title = row[1];
        studios = List.of(row[2].split(", "));
        producers = Arrays.stream(row[3].split(",| and "))
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .toList();
        winner = row[4].equals("yes");
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getStudios() {
        return studios;
    }

    public void setStudios(List<String> studios) {
        this.studios = studios;
    }

    public List<String> getProducers() {
        return producers;
    }

    public void setProducers(List<String> producers) {
        this.producers = producers;
    }

    public Boolean getWinner() {
        return winner;
    }

    public void setWinner(Boolean winner) {
        this.winner = winner;
    }
}

