package renato.alecrim.texo.dto;

import java.util.List;

public class MinMaxWinIntervalDTO {
    private List<ProducerIntervalDTO> min;
    private List<ProducerIntervalDTO> max;

    public MinMaxWinIntervalDTO() {
    }

    /**
     *
     * @param min the producer with the min interval between wins
     * @param max the producer with the max interval between wins
     * @return a new DTO with both producers
     */
    public MinMaxWinIntervalDTO(List<ProducerIntervalDTO> min, List<ProducerIntervalDTO> max) {
        this.min = min;
        this.max = max;
    }

    public List<ProducerIntervalDTO> getMin() {
        return min;
    }

    public void setMin(List<ProducerIntervalDTO> min) {
        this.min = min;
    }

    public List<ProducerIntervalDTO> getMax() {
        return max;
    }

    public void setMax(List<ProducerIntervalDTO> max) {
        this.max = max;
    }
}
