package renato.alecrim.texo.dto;

public class ProducerIntervalDTO {

    private String name;
    private Integer interval;
    private Integer previousWin;
    private Integer followingWin;

    public ProducerIntervalDTO() {
    }

    public ProducerIntervalDTO(String name, Integer previousWin, Integer followingWin, Integer interval) {
        this.name = name;
        this.previousWin = previousWin;
        this.followingWin = followingWin;
        this.interval = interval;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getPreviousWin() {
        return previousWin;
    }

    public void setPreviousWin(Integer previousWin) {
        this.previousWin = previousWin;
    }

    public Integer getFollowingWin() {
        return followingWin;
    }

    public void setFollowingWin(Integer followingWin) {
        this.followingWin = followingWin;
    }
}
