package renato.alecrim.texo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import renato.alecrim.texo.domain.Producer;
import renato.alecrim.texo.dto.MinMaxWinIntervalDTO;
import renato.alecrim.texo.dto.ProducerIntervalDTO;
import renato.alecrim.texo.repository.ProducerRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class ProducerService {
    private final ProducerRepository producerRepository;

    @Autowired
    public ProducerService(ProducerRepository producerRepository) {
        this.producerRepository = producerRepository;
    }

    @Transactional(readOnly = true)
    public List<Producer> findAllProducers() {
        return producerRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Producer findByIdProducer(Integer id) {
        return producerRepository.findById(id)
                .orElse(null);
    }

    @Transactional(readOnly = true)
    public MinMaxWinIntervalDTO getMaxMinWinIntervalProducers() {
        var producers = producerRepository.findAllProducersWithMoreThanOneVictory();
        var minWinners = new ArrayList<ProducerIntervalDTO>();
        var maxWinners = new ArrayList<ProducerIntervalDTO>();

        var minWinner = Integer.MAX_VALUE;
        var maxWinner = Integer.MIN_VALUE;

        for (var p : producers) {
            var yearsWon = Arrays.stream(p.getYearsWon().split("/")).map(Integer::parseInt).sorted().toList();
            for (int i = 1; i < yearsWon.size(); i++) {
                var interval = yearsWon.get(i) - yearsWon.get(i - 1);
                if (interval == minWinner) {
                    minWinners.add(new ProducerIntervalDTO(p.getName(), yearsWon.get(i - 1), yearsWon.get(i), interval));
                } else if (interval < minWinner) {
                    minWinner = interval;
                    minWinners.clear();
                    minWinners.add(new ProducerIntervalDTO(p.getName(), yearsWon.get(i - 1), yearsWon.get(i), interval));
                }
                if (interval == maxWinner) {
                    maxWinners.add(new ProducerIntervalDTO(p.getName(), yearsWon.get(i - 1), yearsWon.get(i), interval));
                } else if (interval > maxWinner) {
                    maxWinner = interval;
                    maxWinners.clear();
                    maxWinners.add(new ProducerIntervalDTO(p.getName(), yearsWon.get(i - 1), yearsWon.get(i), interval));
                }
            }
        }

        return new MinMaxWinIntervalDTO(minWinners, maxWinners);
    }
}
