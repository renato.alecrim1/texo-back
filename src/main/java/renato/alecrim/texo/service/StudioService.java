package renato.alecrim.texo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import renato.alecrim.texo.domain.Studio;
import renato.alecrim.texo.repository.StudioRepository;

import java.util.List;

@Service
@Transactional
public class StudioService {
    private final StudioRepository studioRepository;

    @Autowired
    public StudioService(StudioRepository studioRepository) {
        this.studioRepository = studioRepository;
    }

    @Transactional(readOnly = true)
    public List<Studio> findAllStudios() {
        return studioRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Studio findByIdStudio(Integer id) {
        return studioRepository.findById(id).orElse(null);
    }
}
