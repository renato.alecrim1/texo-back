package renato.alecrim.texo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import renato.alecrim.texo.domain.Movie;
import renato.alecrim.texo.dto.MinMaxWinIntervalDTO;
import renato.alecrim.texo.repository.MovieRepository;
import renato.alecrim.texo.repository.ProducerRepository;

import java.util.List;

@Service
@Transactional
public class MovieService {
    private final MovieRepository movieRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Transactional(readOnly = true)
    public List<Movie> findAllMovies() {
        return movieRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Movie findByIdMovie(Integer id) {
        return movieRepository.findById(id)
                .orElse(null);
    }
}
