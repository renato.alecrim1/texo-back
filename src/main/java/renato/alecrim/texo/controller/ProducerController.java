package renato.alecrim.texo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import renato.alecrim.texo.domain.Producer;
import renato.alecrim.texo.dto.MinMaxWinIntervalDTO;
import renato.alecrim.texo.service.ProducerService;

import java.util.List;

@RestController
@RequestMapping("/public/producer")
public class ProducerController {
    private final ProducerService producerService;

    @Autowired
    public ProducerController(ProducerService producerService) {
        this.producerService = producerService;
    }

    @GetMapping(value = "/producers")
    public List<Producer> findAllProducers() {
        return producerService.findAllProducers();
    }

    @GetMapping(value = "/{id}")
    public Producer findByIdProducer(@PathVariable Integer id) {
        return producerService.findByIdProducer(id);
    }

    @GetMapping("/max-min-win-interval-for-producers")
    public MinMaxWinIntervalDTO getMaxMinWinIntervalForProducers() {
        return producerService.getMaxMinWinIntervalProducers();
    }
}
