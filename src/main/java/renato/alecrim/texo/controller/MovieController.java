package renato.alecrim.texo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import renato.alecrim.texo.domain.Movie;
import renato.alecrim.texo.service.MovieService;

import java.util.List;

@RestController
@RequestMapping("/public/movie")
public class MovieController {
    private final MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping(value = "/movies")
    public List<Movie> findAllMovie() {
        return movieService.findAllMovies();
    }

    @GetMapping(value = "/{id}")
    public Movie findByIdMovie(@PathVariable Integer id) {
        return movieService.findByIdMovie(id);
    }
}
