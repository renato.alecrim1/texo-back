package renato.alecrim.texo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import renato.alecrim.texo.domain.Studio;
import renato.alecrim.texo.service.StudioService;

import java.util.List;

@RestController
@RequestMapping("/public/studio")
public class StudioController {
    private final StudioService studioService;

    @Autowired
    public StudioController(StudioService studioService) {
        this.studioService = studioService;
    }

    @GetMapping(value = "/studios")
    public List<Studio> findAllStudios() {
        return studioService.findAllStudios();
    }

    @GetMapping(value = "/{id}")
    public Studio findByIdStudio(@PathVariable Integer id) {
        return studioService.findByIdStudio(id);
    }
}
