package renato.alecrim.texo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import renato.alecrim.texo.domain.Producer;
import renato.alecrim.texo.dto.ProducerIntervalDTO;
import renato.alecrim.texo.dto.ProducerWinsDTO;

import java.util.List;

@Repository
public interface ProducerRepository extends JpaRepository<Producer, Integer> {
    @Query(value = """
            select p.*, string_agg(m."year", '/') as yearsWon
              from producer p
             inner join producer_movie pm on pm.producer_id = p.id
             inner join movie m on m.id = pm.movie_id
             where m.winner = true
             group by p.id
            having count(p.id) > 1
           """, nativeQuery = true)
    List<ProducerWinsDTO> findAllProducersWithMoreThanOneVictory();
}
