package renato.alecrim.texo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import renato.alecrim.texo.domain.Studio;

@Repository
public interface StudioRepository extends JpaRepository<Studio, Integer> {
}
