package renato.alecrim.texo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import renato.alecrim.texo.domain.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {
}
