package renato.alecrim.texo.domain;

import jakarta.persistence.*;
import renato.alecrim.texo.dto.MovieDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "movie")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "movie_id_gen")
    @SequenceGenerator(name = "movie_id_gen", sequenceName = "movie_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Integer id;

    @Column(name = "\"year\"")
    private Integer year;

    @Column(name = "title")
    private String title;

    @Column(name = "winner")
    private Boolean winner;

    @ManyToMany
    @JoinTable(
            name = "studio_movie",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "studio_id")
    )
    private List<Studio> studios = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "producer_movie",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "producer_id")
    )
    private List<Producer> producers = new ArrayList<>();

    public Movie() {
    }

    public Movie(MovieDTO movieDTO, List<Studio> studios, List<Producer> producers) {
        this.year = movieDTO.getYear();
        this.title = movieDTO.getTitle();
        this.winner = movieDTO.getWinner();
        this.studios = movieDTO.getStudios()
                .stream()
                .map(movieStudio -> studios.stream().filter(studio -> movieStudio.equals(studio.getName())).findAny().orElseThrow())
                .collect(Collectors.toList());
        this.producers = movieDTO.getProducers()
                .stream()
                .map(movieProducer -> producers.stream().filter(producer -> movieProducer.equals(producer.getName())).findAny().orElseThrow())
                .collect(Collectors.toList());

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getWinner() {
        return winner;
    }

    public void setWinner(Boolean winner) {
        this.winner = winner;
    }

    public List<Studio> getStudios() {
        return studios;
    }

    public void setStudios(List<Studio> studios) {
        this.studios = studios;
    }

    public List<Producer> getProducers() {
        return producers;
    }

    public void setProducers(List<Producer> producers) {
        this.producers = producers;
    }

    public Movie merge(Movie movie) {
        this.title = movie.title;
        this.year = movie.year;
        this.studios = movie.studios;
        this.producers = movie.producers;
        this.winner = movie.winner;

        return this;
    }
}
