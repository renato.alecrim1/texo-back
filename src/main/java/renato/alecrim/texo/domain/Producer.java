package renato.alecrim.texo.domain;

import jakarta.persistence.*;

@Entity
@Table(name = "producer")
public class Producer {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "producer_id_gen")
    @SequenceGenerator(name = "producer_id_gen", sequenceName = "producer_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    public Producer() {
    }

    public Producer(String name) {
        this.name = name;
    }

    public Producer(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Producer{" +
                "name='" + name + '\'' +
                '}';
    }

    public Producer merge(Producer producer) {
        this.name = producer.name;
        return this;
    }
}
