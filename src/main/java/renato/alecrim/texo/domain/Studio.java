package renato.alecrim.texo.domain;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "studio")
public class Studio {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "studio_id_gen")
    @SequenceGenerator(name = "studio_id_gen", sequenceName = "studio_id_seq", allocationSize = 1)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    public Studio() {
    }

    public Studio(String name) {
        this.name = name;
    }

    public Studio(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Studio{" +
                "name='" + name + '\'' +
                '}';
    }

    public Studio merge(Studio studio) {
        this.name = studio.name;

        return this;
    }
}
